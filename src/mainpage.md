
 \mainpage Приложение Клиент-сервер.
 
 \section Client Клиенское приложение
 Может работать в двух режимах: в режиме ввода сообщения вручную, или в режиме генерации случайных сообщений. Приложение подключается к серверу по заданному IP через 8888 порт, сериализует сообщение в JSON и отправляет его серверу.
 
 \par Формат запуска:
 $ client IP CODE {MESSAGE | \-\-random}
 
 \par Аргументы:
 	- IP -- IPv4 адрес сервера
 	- CODE -- некоторый индентификатор клиента, который позволит определить, какой именно экземпляр клиента отправил сообщение.
 	- MESSAGE -- Сообщение. Не должено содержать специальных символов, или должно быть закодировано в BASE64
 .
 \par Опции:
 	 - \-\-random -- приложение будет генерировать случайные сообщения и отправлять их серверу. Этот ключ не совместим с аргументом MESSAGE и указывается вместо него.
 
 \par Реализация:
 Функционал сераилизации сообщений и взаимодействия с сервером инкапсулирован в классе Client.
 Для упаковки JSON сообщений используется библиотека RapidJson.
 <br> Для установления соединения и передачи данных используется Unix socket, транспортный протокол -- TCP.
 
 Функция \link client/main.cpp main() \endlink отвечает за распознавание аргументов командной строки, генерацию сообщений и вызов метода класса Client для отправки данных сообщений.

 \section Server Серверное приложение
 Приложение принимает входящие соединения на порту 8888. IPv4 адрес интерфейса, на котором слушает сервер, задается в качестве аргумента командной строки. Приложение принимает данные в формате JSON, которые должны содержать ключи CODE и MESSAGE. Размер JSON не должен превышаться 1 кБ.
 
 Полученные данные записываются в файл СУБД sqlite3 <b>test.db</b>. Приложение создает таблицу messages.
 
 \par Формат запуска:
  $ server IP
 
 \par Аргументы:
 	- IP -- IPv4 адрес сервера
 .
 
 \par Реализация:
 Функционал связанный с приемом соединений и десериализацией JSON инкапсулирован в классе JsonServer. Протокол транспортного уровня -- TCP. Используются множественые слушающие сокеты с установленным флагом SO_REUSEPORT, при этом входящие соединения распределяются сетевой подсистемой Linux таким образом, что одно входящее соединение поступает только на один сокет, что позволяет использовать множество потоков со своим слушающим сокетом.
 
 Метод listen() захватывает управление и внутри класса реализована обработка сигналов завершения. Класс имеет callback для внешнего обработчика сигналов.
 
 Для парсинга JSON сообщений используется библиотека RapidJson. Каждое успешно распарсенное сообщение JSON передается в callback функцию, реализация которой выходит за пределы класса Server. Она реализована в main.cpp в соответствии с требованиями потокобезопасности при использовании общей очереди сообщений, поскольку её вызов внутри server осуществляется многопоточно.
 
 Работа с СУБД sqlite инкапсулирована в классе Database. Функция writer_thread в main.cpp читает сообщения из очереди и записывает их в БД, используя инстанс класса Database. Доступ к очереди так же реализован потокобезопасно.
 
 Функция \link server/main.cpp main() \endlink осуществляет проверку аргумента командной строки, создает инстансы классов Database и JsonServer, после чего объект для работы с БД передается в поток-writer, а управление передается методу listen() JsonSever.
