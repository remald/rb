#include <iostream>
#include <cstring>
#include <cstdlib>
#include <atomic>
#include <csignal>
#include "Client.h"

using namespace std;

//! Сообщение краткой справки
const char *helpmessage = "\n\
client - connect to server and send JSON message(s).\n\
\n\
\e[1mSYNOPSIS\e[0m \n\
client IP CODE {MESSAGE | --random}\n\
\n\
\e[1mDESCRIPTION\e[0m\n\
IP - server ipv4 address (custom port is not supported).\n\
CODE - some code of the client to be written into database. Helps to determine, which client app sent the message.\n\
MESSAGE - message to send, should not contain specail characters, or should be already encoded to BASE64. Will be sent as is.\
\n\
\n\
\e[1mCOMMAND-LINE OPTIONS\e[0m\n\
--random - random messages will be generated at an interval of 1 second. Don't specify a MESSAGE with this option. Must be the last arg.\n\
";

//!Флаговая переменная, true если был получен сигнал завершения
atomic_bool terminated{false};

int main(int argc, char **argv) {

    signal(SIGINT, [](int sig) -> void {
        terminated = true;
        std::cout << "\nreceived interuption signal, exiting!" << std::endl;
    });
    signal(SIGTERM, [](int sig) -> void {
        terminated = true;
        std::cout << "\nreceived termination signal, exiting!" << std::endl;
    });

    bool random = false;
    string ip, code, message;
    /*!В приложение должно быть передано 3 аргумента, либо один аргумент -h. IP сервера, код и сообщение,
     * либо ключ --random если сообщения требуется генерировать случайным образом */
    if (argc == 4) {
        ip = string(argv[1]);
        code = string(argv[2]);
        message = string(argv[3]);

        if (message == string("--random"))
            random = true;

    } else if (argc == 2 && !strcmp("-h", argv[1])) {
        cout << helpmessage << endl;
        return 0;
    } else {
        cout << "\e[1mUsage:\e[0m client IP CODE {MESSAGE | --random} \nUse -h for help" << endl;
        return 0;
    }

    Client client(ip.c_str(), 8888);

    if (-1 == client.connect()) {
        return 1;
    }

    /*! Если введена опция \-\-random, то запускается отправка неограниченного числа случайных сообщений
     * пока не получен сигнал завершения (определяется флагом terminated)*/
    if (random) {

        char msg[64];

        while (!terminated) {
            for (int i = 0; i < 63; i++) {
                msg[i] = char(rand() % 26 + 65);
            }

            client.send_message(code.c_str(), msg);
            sleep(1);
        }

    } else {//!Если флаг случайных сообщений не установлен, то инициируется отправка сообщение пользователя.
        client.send_message(code.c_str(), message.c_str());
    }


    return 0;
}

