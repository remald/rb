//
// Created by mike on 17.03.19.
//

#include <iostream>
#include <thread>
#include <unordered_map>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#ifndef CLIENT_CLIENT_H
#define CLIENT_CLIENT_H

/*!
 * Класс реализует клиент, отправляющий данные упакованные в JSON.
 * 
 * Данный класс инкапсулирует подключение к серверу, сериализацию и отправку данных.
 */
class Client {
private:
    int sock{0}; /*!< Дескриптор сокета для соединения с сервером и отправки данных. */
    uint16_t port; /*!< Номер порта, через который подключается клиент. */
    char ip_address[32]{0};/*!< IP-адрес сервера. */

public:
    /*! \param ip IP-адрес сервера.
     *  \param port номер порта.
     */
    Client(const char *ip, uint16_t port);

    ~Client();

    //! Устанавливает соединение с сервером.
    //! \return 0 в случае успеха, -1 в случае ошибки.
    int connect();
    //!Осуществляет отправку сообщения.
    /*! \param code строка, идентифицирующая клиента.
     *  \param message сообщение, подлежащее отправке. Не должно содержать спецсимволы.
     *  \return 0 в случае успеха, -1 в случае ошибки.
     */
    int send_message(const char *code, const char *message);

};


#endif //CLIENT_CLIENT_H
