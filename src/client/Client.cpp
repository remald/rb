//
// Created by mike on 17.03.19.
//

#include "Client.h"

/*! Устанавливает значения полей класса port и ip_address. */
Client::Client(const char *ip, uint16_t port) {

    this->port = port;
    strcpy(this->ip_address, ip);

}

/*! Устанавливает соединение с сервером по заданному порту и адресу. */
int Client::connect() {

    struct sockaddr_in address;
    struct sockaddr_in serv_addr;

    try {

        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            throw std::runtime_error("Could not create socket!");
        }

        memset(&serv_addr, 0, sizeof(serv_addr));

        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(port);

        if (inet_pton(AF_INET, ip_address, &serv_addr.sin_addr) <= 0) {
            throw std::runtime_error("invalid IP address!");
        }

        if (::connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            throw std::runtime_error("Could not connect!");
        }
    } catch (std::runtime_error &err) {
        std::cerr << "Fatal error: " << err.what() << std::endl;

        return -1;
    }

    return 0;
}


/*! Cериализует код и сообщение в JSON формат и отправляет через сокет клиенту. */
int Client::send_message(const char *code, const char *message) {

    rapidjson::Document d;
    rapidjson::Document::AllocatorType &alloc = d.GetAllocator();
    rapidjson::StringBuffer buffer;

    try {

        d.SetObject();

        d.AddMember("code", rapidjson::Value().SetString(code, alloc), alloc);
        d.AddMember("message", rapidjson::Value().SetString(message, alloc), alloc);

        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        d.Accept(writer);

    } catch (...) {
        std::cerr << "Could not serialize the message!" << std::endl;
        return 1;
    }


    int n = send(sock, buffer.GetString(), buffer.GetLength(), 0);
    if (n > 0)
        std::cout << "Message sent successfully!" << std::endl;
    else
        std::cout << "Could not send a message!" << std::endl;

    return 0;
}

Client::~Client() {
    if (sock > 0) {
        shutdown(sock, SHUT_RDWR);
        close(sock);
    }
}



