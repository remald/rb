

#include <iostream>
#include <thread>
#include <unordered_map>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <csignal>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/eventfd.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <atomic>
/* сторонняя встраиваемая библиотека для обработки JSON */
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#ifndef SERVER_WORKER_H
#define SERVER_WORKER_H

/*! Количество потоков-воркеров. */
#define THREADS_COUNT 4

/*! Максимальное число соединений на один поток. */
#define MAX_CONN 2500
/*! Максимальная длина json. */
#define MAXLEN 128
/*! Максимальное число возвращемых epoll событий. */
#define MAX_EVENTS 128

/*! Тип callback для обработки полученных из JSON данных. */
using data_callback_t = void (*)(const rapidjson::Document &dom);
/*! Тип callback для обработки сигнала*/
using signal_callback_t = void (*)(int signal);

/*! Класс JsonServer инкапсулирует сервер, принимающий соединения TCP и данные в формате JSON.
 * Сервер находит в потоке tcp JSON, парсит, и передает в обработчик, установленный пользователем класса. */
class JsonServer {
private:

    uint16_t port{8888};
    char ip[32];
    //! Дескриптор события завершения.
    volatile static int terminate;
    std::string errormsg;

    /*! Структура <b>struct client_t</b> хранит данные клиента. Полученное на настоящий момент сообщение, в котором еще не найден JSON, текущий размер сообщения, количество открывающих и закрывающих скобок. */
    struct client_t {
        char message[MAXLEN + 1]{0};
        size_t size{0};
        unsigned char lpar_count{0};
        unsigned char rpar_count{0};
    };

    //! Указатель на пользовательский обработчик JSON данных.
    data_callback_t rq_handler;

    //! Указатель на пользовательский обработчик сигналов.
    static signal_callback_t sig_callback;

    //! Внутренний обработчик сигналов.
    /*! \param sig номер сигнала Linux*/
    static void signalhandler(int sig);

    //! Записывает в структуру порт и адрес.
    /*! \param addr указатель на структуру для записи
        \param port порт
        \param ip4 -- адрес IPv4*/
    void set_sockaddr(sockaddr_in *addr, const uint16_t &port, const char *ip4);

    //! Устанавливает сокет неблокирующим.
    /*! \param sock дескриптор сокета */
    void setnonblocking(int sock);

    //! Добавляет в epoll дескриптор.
    /*! \param epoll_ дескриптор epoll
        \param fd дескриптор для добавления
        \param events флаги событий, на которые подписывается epoll*/
    void epoll_ctl_add(int epoll_, int fd, uint32_t events);

    //! Принимает входящее соединение и добавляет его в epoll.
    /*! \param epoll_ дескриптор epoll
        \param listen_sock дескриптор слушающего сокета, на который поступило входящее соединение*/
    int accept_client(int epoll_, int listen_sock);

    //! Основной метод, реализующий потоки, принимающие и обрабатывающие соединения.
    void client_thread();

    //! Завершает соединение, закрывает сокет, удаляет структуру клиента и его дескриптор из списка
    /*! \param epoll_ дескриптор epoll
        \param sock дескриптор сокета
        \param clients список клиентов, содержит структуру client_t по ключу - дескриптору сокета. */
    void close_client(int epoll_, int sock, std::unordered_map<int, client_t *> &clients);

    //! Пытается распарсить сообщение как JSON
    /*! \param maybe_json строка, содержащая предварительно выделенную структуру JSON
        \param n размер строки */
    void try_json(const char *maybe_json, size_t n);


public:
    JsonServer() = default;

    ~JsonServer() = default;

    //! Метод инициирует прослушивание заданного порта по данному IP.
    /*! \param port порт
     *	\param addr IPv4 адрес */
    void listen(const uint16_t &port, const char *addr);

    //! Устанавливает указатель на callback для обработки полученных JSON объектов.
    void setDataCallback(data_callback_t f);

    //! Устанавливает указатель на callback для внешнего обработчика сигналов.
    static void setSigCallback(signal_callback_t f);
};


#endif //SERVER_WORKER_H
