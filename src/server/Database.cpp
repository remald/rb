
#include <cstring>
#include "Database.h"

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
    int i;
    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

/*! Конструктор класса*/
Database::Database() {

    try {
        char *errorMsg = nullptr;
        //! Открываем файл базы данных.
        int rc = sqlite3_open("test.db", &db);

        if (rc) {
            throw std::runtime_error(std::string("Can't open database: ") + sqlite3_errmsg(db));
        }

        //! Создается таблица messages, если она не существует. Первичный ключ id инкрементируется автоматически.
        const char sql[] = "CREATE TABLE IF NOT EXISTS messages (\
		id INTEGER PRIMARY KEY AUTOINCREMENT,\
		code VARCHAR(32) NOT NULL DEFAULT '',\
		message VARCHAR(128) NOT NULL DEFAULT ''\
		)";

        rc = sqlite3_exec(db, sql, callback, nullptr, &errorMsg);

        if (rc != SQLITE_OK) {
            sqlite3_free(errorMsg);
            throw std::runtime_error(std::string("SQL error: ") + sqlite3_errmsg(db));
        }

        sqlite3_free(errorMsg);
        //! Подготовливается Prepared statement
        if (sqlite3_prepare(db, "insert into messages (code, message) values (?,?)", -1, &stmt, nullptr)
            != SQLITE_OK) {

            throw std::runtime_error(std::string("Could not prepare statement: ") + sqlite3_errmsg(db));

        }


        init_success = true;

    } catch (std::runtime_error &e) {
        //! Если инициализация БД не удалась, устанавливается флаг init_success в false
        init_success = false;
        std::cerr << "Fatal error in Database(): " << e.what() << std::endl;
    }

}


/*! В деструкторе обеспечивается корректное закрытие подключения к базе */
Database::~Database() {

    sqlite3_finalize(stmt);
    sqlite3_close(db);

}

void Database::write(const char *code, const char *message) {

    try {

        if (sqlite3_bind_text(stmt, 1, code, strlen(code), nullptr)
            || sqlite3_bind_text(stmt, 2, message, strlen(message), nullptr)) {
            throw "Could not bind parameters.";
        }

        if (sqlite3_step(stmt) != SQLITE_DONE) {
            throw "Could not step (execute) stmt.\n";
        }

    } catch (const char *err) {
        std::cerr << "Error while adding data to DB: " << err << std::endl;
    }

    sqlite3_clear_bindings(stmt);
    sqlite3_reset(stmt);

}
