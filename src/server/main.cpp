
#include <iostream>
#include <csignal>
#include <queue>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "JsonServer.h"
#include "Database.h"

using namespace std;

#define loop while(1)
//! Максимальное число объектов JSON в очереди.
#define MAX_BUFFERSIZE 512
//! Очередь обмена между пуллом потоков worker и потоком writer.
std::queue<string> buffer;
//! Флаговая переменная завершения работы приложения.
std::atomic<bool> terminated{false};
//! Мьютекс для обеспечения атомарности доступа к общему ресурсу -- очереди, а так же связанным с ней переменным.
std::mutex buffer_lock;
//! Условная переменная для синхронизации работы потоков в связи с освобождением очереди.
std::condition_variable data_removed;
//! Условная переменная для синхронизации работы потоков в связи с появлением данных в очереди.
std::condition_variable data_added;
//! Число сообщений JSON в очереди.
int buffer_size{0};

/*! Функция callback для объекта класса JsonServer. Функция вызывается любым потоком из пулла, когда
 * им был получен корректный JSON.
 \param  const rapidjson::Document &dom -- объект, содержащий JSON */
void worker_threads_callback(const rapidjson::Document &dom) {

    try {
        if (!dom.HasMember("code") || !dom.HasMember("message"))
            throw std::runtime_error("not all the obligatory fields were found!");
        if (!dom["code"].IsString() || !dom["message"].IsString())
            throw std::runtime_error("id and message must have a string type!");
    } catch (std::runtime_error &err) {
        std::cout << "Can not parse JSON: " << err.what() << std::endl;
        return;
    }

    {//! Входит в критическую секцию.
        std::unique_lock<std::mutex> lk(buffer_lock);
        if (buffer_size >=
            MAX_BUFFERSIZE) {//! Если в очереди слишком много данных, ждет когда поток-writer запишет часть данных в БД.
            data_removed.wait(lk, [] { return buffer_size < MAX_BUFFERSIZE || terminated; });
        }

        if (terminated)
            return;

        buffer.push(dom["code"].GetString());
        buffer.push(dom["message"].GetString());
        buffer_size++; //!Добавляет данные в очередь.

        lk.unlock();
        data_added.notify_one();//! Посылает через условную переменную data_added уведомление о том, что данные добавлены.
    }
}

/*! Функция релизует поток записи данных в БД. 
 \param unique_ptr <DataBase> pDb - умный указатель на инстанс DataBase()*/
void writer_thread(unique_ptr<Database> pDb) {

    string code, message;

    loop {//! Работает в цикле, пытаясь войти в критическую секцию и прочитать данные из очереди, ожидая на условной переменной в случае если очередь пуста. Если в очереди появляются данные и отправлено уведомление, он активируется и забирает данные из очереди.

        {
            std::unique_lock<std::mutex> lk(buffer_lock);
            if (buffer_size < 1)
                data_added.wait(lk, [] { return buffer_size > 0 || terminated; });

            if (terminated)
                break;

            code = buffer.front();
            buffer.pop();
            message = buffer.front();
            buffer.pop();

            buffer_size--;

            lk.unlock();
            data_removed.notify_all();
        }

        pDb->write(code.c_str(), message.c_str());//! С помощью инстанса класса DataBase данные записываются в БД.
    }
}


int main(int argc, char **argv) {

    if (argc != 2 || inet_addr(argv[1]) == -1) {
        std::cerr <<
                  "\e[1musage:\e[0m server IP_ADDR\n"
                  "Where IP_ADDR -- ipv4 address to listen to. Custom port is not supported."
                  << std::endl;

        return 0;
    }

    //! Указатель <b>pDb</b> на объект класса DataBase реализующий работу с SQLite.
    unique_ptr<Database> pDb(new Database);

    if (!pDb->isInitSuccess()) {
        return 1;
    }

    //! Указатель <b>server</b> на объект класса JsonServer реализующий многопоточный JSON сервер. Принимает по tcp данные от множества клиентов, выделяет из них отдельные JSON сообщения и передает на обработку в callback.
    unique_ptr<JsonServer> server(new JsonServer);

    //! Лямбда-функция -- обработчик сигналов модифицирует флаг завершения потока writer и посылает уведомление через условную переменную.
    server->setSigCallback([](int sig) -> void {
        terminated = true;
        data_removed.notify_all();
        data_added.notify_one();
    });
    //! В качестве callback для обработки сообщений из server устанавливается функция worker_threads_callback().
    server->setDataCallback(worker_threads_callback);
    //! Создается поток записи в БД, в котором запускается функция writer_thread().
    std::thread th(writer_thread, std::move(pDb));
    //! Управление передается методу listen() класса JsonServer.
    server->listen(8888, argv[1]);

    th.join();

    return 0;
}
