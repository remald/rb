
#include <cstring>
#include <csignal>
#include "JsonServer.h"

/****************PRIVATE MEMBERS****************/


volatile int JsonServer::terminate = eventfd(0, 0);
signal_callback_t JsonServer::sig_callback = nullptr;

/*! Триггерит событие завершения для потоков, а затем пытается вызват callback переданный пользователем */
void JsonServer::signalhandler(int sig) {
    if (sig == SIGINT)
        std::cout << "\nreceived SIGINT, exiting!" << std::endl;
    if (sig == SIGTERM)
        std::cout << "\nreceived SIGTERM signal, exiting!" << std::endl;

    eventfd_write(terminate, 1);

    if (sig_callback)
        (*sig_callback)(sig);

}

void JsonServer::set_sockaddr(sockaddr_in *addr, const uint16_t &port, const char *ip4) {
    memset((char *) addr, 0, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    if ((addr->sin_addr.s_addr = inet_addr(ip4)) == -1)
        throw std::runtime_error("Incorrect IP address!");
    addr->sin_port = htons(port);
}

void JsonServer::setnonblocking(int sock) {
    if (fcntl(sock, F_SETFL, fcntl(sock, F_GETFD, 0) | O_NONBLOCK) == -1) {
        throw std::runtime_error(std::string("setnonblocking() fcntl(): ") + strerror(errno));
    }
}

void JsonServer::epoll_ctl_add(int epoll_, int fd, uint32_t events) {
    struct epoll_event ev{0};
    ev.events = events;
    ev.data.fd = fd;
    if (epoll_ctl(epoll_, EPOLL_CTL_ADD, fd, &ev) == -1) {
        throw std::runtime_error(std::string("epoll_ctl(): ") + strerror(errno));
    }
}

void JsonServer::close_client(int epoll_, int sock, std::unordered_map<int, client_t *> &clients) {

    if (clients.find(sock) != clients.end()) {
        epoll_ctl(epoll_, EPOLL_CTL_DEL,
                  sock, NULL);

        shutdown(sock, SHUT_RDWR);
        close(sock);

        delete (clients[sock]);
        clients.erase(sock);

        printf("[+] connection closed\n");
    }

}

/*! Исключения в данной функции отлавливаются отдельно, так как функция работает с входящими соединениями,
 * ошибка на данном этапе не должна приводить к завершению работы сервера. При этом выводится сообщение об ошибке. */
int JsonServer::accept_client(int epoll_, int listen_sock) {

    char buf[256]{0};
    struct sockaddr_in cli_addr;
    socklen_t socklen = sizeof(cli_addr);
    int conn_sock = -1;

    try {

        conn_sock = accept(listen_sock,
                           (struct sockaddr *) &cli_addr, &socklen);

        if (conn_sock < 1)
            throw std::runtime_error(std::string("accept(): ") + strerror(errno));

        inet_ntop(AF_INET, (char *) &(cli_addr.sin_addr),
                  buf, sizeof(cli_addr));
        printf("[+] connected with %s:%d\n", buf,
               ntohs(cli_addr.sin_port));

        setnonblocking(conn_sock);
        epoll_ctl_add(epoll_, conn_sock,
                      EPOLLIN | EPOLLRDHUP |
                      EPOLLHUP | EPOLLET);

        return conn_sock;

    } catch (std::runtime_error &e) {

        std::cerr << "Error: " << e.what() << std::endl;
        if (conn_sock > 0) {
            shutdown(conn_sock, SHUT_RDWR);
            close(conn_sock);
        }

        return 1;
    }
}

/*! Реализует потоки для обработки клиентов. Потоки работают асинхронно, имея свои собственные дескрипторы
 * и структуры для хранения промежуточных данных, использование mutex по этой причине не требуется.
 * У каждого потока есть собественный слушающий сокет, объявленый с флагом SO_REUSEPORT,
 * что позволяет открывать на одном порту множество слушающих сокетов.
 * При этом принимает входящее соединение только один из потоков, ядро синхронизирует использование сокетов SO_REUSEPORT. */

void JsonServer::client_thread() {
    //! Словарь <b>clients</b> хранит данные клиентов и позволяет получить доступ по номеру дескриптора буфер для кадждого подключенного клиента. Поскольку через TCP соединение валидный JSON в некоторых ситуациях может быть получен по частям, требуется хранить промежуточные результаты для каждого из клиентов.
    std::unordered_map<int, client_t *> clients;
    //! <b>events</b> -- структура, куда будут попадать события из epoll.
    struct epoll_event events[MAX_EVENTS];
    int nfds;
    ssize_t n;
    //! <b>listen_sock</b> -- cлушающий сокет.
    int listen_sock = -1;
    int epoll_ = -1;

    struct sockaddr_in srv_addr;
    //! Код инициализации слушающего сокета помещен в блок try, инициализирует слушающий сокет и запускаем его прослушивание.
    try {

        if ((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
            throw std::runtime_error(std::string("socket(): ") + strerror(errno));

        int enable = 1; //! Используется флаг SO_REUSEPORT.
        if (setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, sizeof(int)))
            throw std::runtime_error(std::string("setsockopt(): ") + strerror(errno));

        set_sockaddr(&srv_addr, port, ip);

        if (bind(listen_sock, (struct sockaddr *) &srv_addr, sizeof(srv_addr)))
            throw std::runtime_error(std::string("bind(): ") + strerror(errno));

        setnonblocking(listen_sock);

        if (::listen(listen_sock, MAX_CONN))
            throw std::runtime_error(std::string("listen(): ") + strerror(errno));

        epoll_ = epoll_create(1);
        //! Добавляет в epoll дискриптор события завершения программы.
        epoll_ctl_add(epoll_, terminate, EPOLLIN);

        //! Добавляет слушающий сокет.
        epoll_ctl_add(epoll_, listen_sock, EPOLLIN | EPOLLOUT);


        //! В данный блок catch попадут исключения связанные со слушающими сокетами, и если на данном этапе возникла ошибка, то дальнейшая работа приложения не возможна. Поэтому инициируется завершение работы приложения.

    } catch (std::runtime_error &e) {
        errormsg = std::string("Fatal error in a worker thread: ") + e.what() + "\n";

        signalhandler(-1);

        return;
    }

    //! <b>terminated</b> -- локальный флаг для завершения цикла.
    bool terminated{false};
    //! Цикл неограниченное число раз запускает ожидание epoll_wait, пока не установлен флаг завершения.
    while (!terminated) {

        memset(events, 0, MAX_EVENTS * sizeof(struct epoll_event));
        //! <b>epoll_wait()</b> ожидает события.
        nfds = epoll_wait(epoll_, events, MAX_EVENTS, -1);
        //! Цикл производит итерацию по всем событиям, возвращенным epoll_wait.
        for (int i = 0; i < nfds; i++) {
            //! В первую очередь обработывается событие завершения. Устанавливется флаг и цикл прерывается, если получено это событие.
            if (events[i].data.fd == terminate) {
                terminated = true;
                break;
            }
            if (events[i].data.fd == listen_sock) {
                //! Если получено событие от слушающиего сокета, принимаем соединение и добавляем так же в epoll.
                int conn_sock = accept_client(epoll_, listen_sock);

                if (conn_sock != -1) clients[conn_sock] = new client_t;

            } else if (events[i].events & EPOLLIN) {
                //! Если получены данные от одного из клиентов, считываем эти данные и проводим предварительную проверку JSON подсчетом скобок. Если предполагаемый JSON найден -- вызываем try_json для его парсинга. Удаляем затем переданные данные из буфера клиента.
                client_t *client = clients[events[i].data.fd];
                while ((n = read(events[i].data.fd, client->message + client->size, MAXLEN - client->size))
                       > 0) {
                    for (int j = 0; j < n; j++) {
                        if (client->message[client->size + j] == '{') client->lpar_count++;
                        if (client->message[client->size + j] == '}') client->rpar_count++;
                        if (client->lpar_count &&
                            client->lpar_count == client->rpar_count) {//обнаружено соответствие скобок

                            char tmp = client->message[client->size + j + 1];
                            client->message[client->size + j + 1] = 0;
                            try_json(client->message, client->size + j + 1);
                            client->message[client->size + j + 1] = tmp;

                            if (j + 1 < n)
                                memcpy(client->message, client->message + client->size + j + 1, (size_t) n - j - 1);

                            n = n - j - 1;
                            j = -1;
                            client->size = 0;

                            client->rpar_count = client->lpar_count = 0;
                        }
                    }

                    client->size += n;
                    //! Если достигнут максимальный размер буфера, и JSON не обанаружен, то разрываем соединение с клиентом, он передает некорректные данные.
                    if (client->size >= MAXLEN) {
                        std::cerr
                                << "Buffer limit reached, no valid json found! This connection will be terminated."
                                << std::endl;
                        close_client(epoll_, events[i].data.fd, clients);
                    }
                }
            }
            //! Если клиент закрыл соедиение -- вызываем функцию очистки данных и закрытия дескрипторов клиента.
            if (events[i].events & (EPOLLRDHUP | EPOLLHUP)) {
                close_client(epoll_, events[i].data.fd, clients);
                continue;
            }
        }
    }
    //! Когда цикл завершился, то есть был установлен флаг terminate, удаляем все структуры и закрываем дескрипторы, программа будет завершена.

    if (listen_sock > 0) {
        epoll_ctl(epoll_, EPOLL_CTL_DEL,
                  listen_sock, NULL);

        shutdown(listen_sock, SHUT_RDWR);
        close(listen_sock);
    }

    for (auto client: clients) {
        epoll_ctl(epoll_, EPOLL_CTL_DEL,
                  client.first, NULL);

        shutdown(client.first, SHUT_RDWR);
        close(client.first);
        delete (client.second);
    }

}


void JsonServer::try_json(const char *maybe_json, size_t n) {


    rapidjson::Document dom;
    dom.Parse(maybe_json);
    if (dom.HasParseError()) {
        std::cout << "Can not parse JSON: " << "invalid format!" << std::endl;
        return;
    }
    //! Если удалось распарсить данные, передаем полученную структуру в callback, который установлен пользователем.
    (*rq_handler)(dom);

}


/****************PUBLIC MEMBERS****************/


/*! Запускает пулл потоков, слушающих указанный порт. */
void JsonServer::listen(const uint16_t &port, const char *addr) {

    this->port = port;
    strcpy(this->ip, addr);

    std::thread threads[THREADS_COUNT];

    for (int i = 0; i < THREADS_COUNT; i++) {
        threads[i] = std::thread(&JsonServer::client_thread, this);
    }

    //! Устанавливаем обработчики сигналов завершения.
    std::signal(SIGINT, JsonServer::signalhandler);
    std::signal(SIGTERM, JsonServer::signalhandler);

    for (int i = 0; i < THREADS_COUNT; i++) {

        threads[i].join();

    }

    std::cout << errormsg << std::endl;
}

void JsonServer::setDataCallback(data_callback_t f) {

    rq_handler = f;

}

void JsonServer::setSigCallback(signal_callback_t f) {

    sig_callback = f;

}

