
#ifndef SERVER_WRITER_H
#define SERVER_WRITER_H

#include <iostream>
#include <sqlite3.h>

/*! Класс инкапсулирует обработку базы данных. */
class Database {
private:
    sqlite3_stmt *stmt{NULL};
    sqlite3 *db = NULL;

    //! Флаг успеха инициализации БД. true если инициализация успешна, false в противном случае.
    bool init_success{false};


public:
    Database();

    ~Database();

    /*! Метод осуществляет запись данных в БД.
     * \param const char *code -- идентификатор клиента
     * \param const char *message -- сообщение, не содержащее специальных символов
     */
    void write(const char *code, const char *message);

    /*! Метод возвращает результат инициализации БД.
     * \return значение true если инициализация успешна, в противном случае false.
     */
    bool isInitSuccess() { return init_success; }

};


#endif //SERVER_WRITER_H
