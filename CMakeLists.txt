cmake_minimum_required(VERSION 3.0)
project(client_server)
set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build)

add_custom_target(rapidjson)
add_custom_command(
        TARGET rapidjson PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
                ${CMAKE_SOURCE_DIR}/3rd_party/rapidjson/include/
                ${CMAKE_SOURCE_DIR}/3rd_party/build/include/
)

add_custom_target(dep ALL DEPENDS rapidjson)

include(ExternalProject)
ExternalProject_Add(
  libsqlite3
  SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/sqlite3
  CONFIGURE_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/sqlite3/configure --prefix=${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/build
  PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/sqlite3
  BUILD_COMMAND make && make install
  BUILD_IN_SOURCE 1
)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

include_directories(${CMAKE_SOURCE_DIR}/3rd_party/build/include)
link_directories(${CMAKE_SOURCE_DIR}/3rd_party/build/lib)

add_subdirectory(src/server)
add_subdirectory(src/client)

